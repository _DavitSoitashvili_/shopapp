@extends("dashboard.dashboard_layout")
@section("Title")
    Create New Post
@endsection

@section("Content")
    <div style="width: 400px; margin: 100px auto;" id="create_post">
        <form method="post" action="{{route("save.post")}}">
            @csrf
            <div class="mb-3">
                <label for="title" class="form-label">Title</label>
                <input name="title" type="text" class="form-control" id="title">
                <span class="alert-danger">{{$errors->first("title")}}</span>
            </div>
            <div class="mb-3">
                <label class="form-check-label" for="description">Description</label>
                <input type="text" name="description" class="form-control" id="description">
                <span class="alert-danger">{{$errors->first("description")}}</span>
            </div>
            <div class="mb-3">
                <label for="image_url" class="form-label">Image Url</label>
                <input name="image_url" type="text" class="form-control" id="image_url" aria-describedby="emailHelp">
                <span class="alert-danger">{{$errors->first("image_url")}}</span>
            </div>
            <div class="mb-3">
                <label for="price" class="form-label">Price</label>
                <input name="price" type="number" class="form-control" id="price" aria-describedby="emailHelp">
                <span class="alert-danger">{{$errors->first("price")}}</span>
            </div>
            <button type="submit" class="btn btn-primary">Create</button>
        </form>
    </div>
@endsection
