@extends("dashboard.dashboard_layout")
@section("Title")
    Posts
@endsection

@section("Content")
    <div style="margin: auto; width: 1480px">
        @foreach($posts as $post)
            <div style="margin: 30px;display: inline-block; width: 300px; height: 200px" class="post">
                <a href="/post-details/{{$post->id}}"><img style="width: 300px; height: 200px; border-radius: 20px"
                                                           src="{{$post->image_url}}" alt="error..."></a>
                <h3 style="color: red">Title: {{$post->title}}</h3>
                <p style="color: green; text-overflow: ellipsis;white-space: nowrap;overflow: hidden;width:300px; font-size: 20px">
                    Description: {{$post->description}}</p>
                <p style="font-size: 20px; color: black">Price : {{$post->price}}$</p>
                <p style="font-size: 20px; color: blue">Author: {{$post->author}}</p>
                <p style="font-size: 18px; color: orangered">Creation Date: {{$post->created_at}}</p>
                <p style="font-size: 20px; font-weight: bold; color: purple">Likes : {{sizeof($post->likes)}}</p>
                @if($isMyPosts == true)
                    <div style="display: flex">
                        <form method="get" action="{{route("edit.post", $post->id)}}">
                            @csrf
                            <button style="width:50px" class="btn btn-success" type="submit">Edit</button>
                        </form>
                        <form method="post" action="{{route("delete.post", $post->id)}}">
                            @csrf
                            @method("DELETE")
                            <button style="width:50px; margin-left: 5px" class="btn btn-danger" type="submit">-</button>
                        </form>
                    </div>
                @endif
            </div>
        @endforeach
    </div>
@endsection
