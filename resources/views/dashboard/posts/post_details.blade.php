@extends("dashboard.dashboard_layout")
@section("Title")
    Post Details
@endsection

@section("Content")
    <div id="post_details" style="display: flex">
        <div style="margin:50px auto; width: 400px; height: 200px" class="post">
            <img style="width: 300px; height: 200px; border-radius: 20px" src="{{$post->image_url}}"
                 alt="error...">
            <h3 style="color: red">Title: {{$post->title}}</h3>
            <p style="color: green; font-size: 20px">
                Description: {{$post->description}}</p>
            <p style="font-size: 20px; color: black">Price : {{$post->price}}$</p>
            <p style="font-size: 20px; color: blue">Author: {{$post->author}}</p>
            <p style="font-size: 18px; color: orangered">Creation Date: {{$post->created_at}}</p>

            @if($isLiked == true)
                <form method="get" action="{{route("dislike.post", $post->id)}}">
                    <button type="submit" class="btn btn-danger">Dislike</button>
                </form>
            @else
                <form method="get" action="{{route("like.post", $post->id)}}">
                    <button type="submit" class="btn btn-primary">Like</button>
                </form>
            @endif
        </div>
        <div>
            <h3 style="color: red; margin-left: 270px; margin-top: 50px">Comments</h3>
            <div id="comments"
                 style="height:300px; width:500px; border:1px solid #ccc;overflow:auto; margin:50px 100px;">
                @foreach($comments as $comment)
                    <div class="comment">
                        <p style="color: green; font-size: 18px">{{$comment->author}} : {{$comment->message}}</p>
                        <p style="color: black">Created At : {{$comment->created_at}}</p>
                        <hr>
                    </div>
                @endforeach
            </div>
            <div style="display: block; width: 300px; margin-left: 100px"
                 id="send-comment">
                <form method="post" action="{{route("send.comment"), $post->id}}">
                    @csrf
                    <input type="number" hidden="hidden" value="{{$post->id}}" name="post_id">
                    <input required name="message" placeholder="type comment.." type="text" class="form-control"
                           id="message">
                    <button style="margin-top: 10px; margin-bottom: 30px" type="submit" class="btn btn-success">Send
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
