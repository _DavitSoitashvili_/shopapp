@extends("auth.auth_layout")
@section("Title")
    Sign In
@endsection

@section("Content")
    <div style="width: 300px; height: 400px; margin: 200px auto" id="sign_in">
        <form method="post" action="{{route(("signin.user"))}}">
            @csrf
            <div class="mb-3">
                <label for="email" class="form-label">Email address</label>
                <input name="email" type="email" class="form-control" id="email" aria-describedby="emailHelp">
                <span class="alert-danger">{{$errors->first("email")}}</span>
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input name="password" type="password" class="form-control" id="password">
                <span class="alert-danger">{{$errors->first("password")}}</span>
            </div>
            <button type="submit" class="btn btn-primary">Sign In</button>
        </form>
        <form style="margin-top: 10px" method="get" action="{{route("signup")}}">
            <button class="btn btn-success" type="submit">Sign Up</button>
        </form>
    </div>
@endsection
