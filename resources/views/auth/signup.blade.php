@extends("auth.auth_layout")
@section("Title")
    Sign Up
@endsection

@section("Content")
    <div style="width: 300px; margin: 200px auto" id="sign_up">
        <form method="post" action="{{Route("signup.user")}}">
            @csrf
            <div class="mb-3">
                <label for="email" class="form-label">Email address</label>
                <input name="email" type="email" class="form-control" id="email" aria-describedby="emailHelp">
                <span class="alert-danger">{{$errors->first("email")}}</span>
            </div>
            <div class="mb-3">
                <label for="Password" class="form-label">Password</label>
                <input name="password" type="password" class="form-control" id="Password">
                <span class="alert-danger">{{$errors->first("password")}}</span>
            </div>
            <div class="mb-3">
                <label for="Confirm_Password" class="form-label">Confirm Password</label>
                <input name="confirm_password" type="password" class="form-control" id="Confirm_Password">
                <span class="alert-danger">{{$errors->first("confirm_password")}}</span>
            </div>
            <button type="submit" class="btn btn-primary">Sign Up</button>
        </form>
        <form style="margin-top: 10px" method="get" action="{{route("signin")}}">
            <button class="btn btn-success" type="submit">Sign In</button>
        </form>
    </div>
@endsection
