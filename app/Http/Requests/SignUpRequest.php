<?php


namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class SignUpRequest extends FormRequest
{
    public function rules()
    {
        return   [
            "email" => 'required',
            "password" => 'required|min:5',
            "confirm_password" => 'required|same:password|min:5'
        ];
    }
}
