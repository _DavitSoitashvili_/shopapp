<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    public function rules()
    {
        return [
            "title" => 'required|min:5',
            "description" => 'required|min:8',
            "image_url" => 'required|min:7',
            "price" => 'required',
        ];
    }
}
