<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Comment;
use App\Models\Like;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PostsController extends Controller
{
    function posts()
    {
        if (Auth::user() == null) {
            return redirect("/signin");
        }
        $isMyPosts = false;
        $posts = Post::all();
        return View("dashboard.posts.show_posts", compact("posts", "isMyPosts"));
    }

    function myPosts()
    {
        $isMyPosts = true;
        $user = $this->getCurrentUser();
        $posts = Post::select("*")->where("author", "=", $user->email)->get();
        return View("dashboard.posts.show_posts", compact("posts", "isMyPosts"));
    }

    function createPost()
    {
        return View("dashboard.posts.create_post");
    }

    function editPost($id)
    {
        $post = $this->getPost($id);
        return View("dashboard.posts.edit_post", compact("post"));
    }

    function saveEditedPost(PostRequest $request, $id)
    {
        $post = $this->getPost($id);
        $this->updatePostFields($post, $request);
        $post->save();
        $this->sendLoggerNotification("The post " . $post->title . " Has been edited!");
        return redirect("/my-posts");
    }

    private function updatePostFields(Post $post, Request $request)
    {
        $post->title = $request->get("title");
        $post->description = $request->get("description");
        $post->image_url = $request->get("image_url");
        $post->price = $request->get("price");
    }

    function deletePost($id)
    {
        $post = $this->getPost($id);
        $post->delete();
        $this->sendLoggerNotification("Post: " . $post . " has been deleted!");
        return redirect("/my-posts");
    }

    function savePost(PostRequest $request)
    {
        $user = $this->getCurrentUser();
        $post = new Post();
        $this->updatePostFields($post, $request);
        $post->author = $user->email;
        $this->sendLoggerNotification("New Post: " . $post . " Has been created by " . $user->email);
        $post->save();
        return redirect("/");
    }

    function likePost($id)
    {
        $user = $this->getCurrentUser();
        $like = new Like();
        $like->is_liked = true;
        $like->author = $user->email;
        $like->post_id = $id;
        $like->save();

        return redirect("/");
    }

    function dislikePost($id)
    {
        $user = $this->getCurrentUser();
        $like = Like::select("*")->where("author", "=", $user->email)->where("post_id", "=", $id)->get()[0];
        $like->delete();
        return redirect("/");
    }

    function signOut()
    {
        Auth::logout();
        return redirect("/");
    }

    function postDetails($id)
    {
        $post = $this->getPost($id);
        $user = $this->getCurrentUser();
        $isLiked = false;
        $likes = $post->likes;

        foreach ($likes as $like) {
            if ($like->author == $user->email) {
                $isLiked = true;
                break;
            }
        }
        $comments = $post->comments;
        return View("dashboard.posts.post_details", compact("post", "comments", "isLiked"));
    }

    static function getPost($id)
    {
        return Post::findOrFail($id);
    }

    function getCurrentUser()
    {
        return User::findOrFail(Auth::user()->getAuthIdentifier());
    }

    function sendComment(Request $request)
    {
        $postId = $request->get("post_id");
        $user = $this->getCurrentUser();
        $comment = new Comment();
        $comment->message = $request->get("message");
        $comment->author = $user->email;
        $comment->post_id = $postId;
        $comment->save();

        $this->sendLoggerNotification("New Comment: " . $comment->message . " Has been added by: " . $user->email);

        return redirect("/post-details/" . $postId);

    }

    private function sendLoggerNotification($message)
    {
        Mail::raw($message, function ($message) {
            $message->to("shopapp@yahoo.com");
        });
    }
}
