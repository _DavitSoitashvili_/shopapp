<?php

namespace App\Http\Controllers;

use App\Http\Requests\SignInRequest;
use App\Http\Requests\SignUpRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    function signIn()
    {
        if (Auth::user() != null) {
            return redirect("/");
        }
        return View("auth.signin");
    }


    function signInUser(SignInRequest $request)
    {
        $credentials = $request->except("_token");
        if (Auth::attempt($credentials)) {
            return redirect("/");
        } else {
            return abort(403);
        }
    }

    function signUp()
    {
        if (Auth::user() != null) {
            return redirect("/");
        }
        return View("auth.signup");
    }

    function signUpUser(SignUpRequest $request)
    {
        $email = $request->get("email");
        $password = $request->get("password");
        $this->createNewUser($email, $password);
        return redirect("/");
    }

    static function createNewUser($email, $password)
    {
        $user = new User();
        $user->name = $email;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->save();
    }

}
