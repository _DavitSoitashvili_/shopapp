<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\PostsController;
use App\Models\Post;
use Illuminate\Http\Request;

class PostsApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function posts()
    {
        $posts = Post::all();
        return response($posts);
    }

    public function postComments($id)
    {
        $post = PostsController::getPost($id);
        $comments = $post->comments;
        return response(["post" => $post, "comments" => $comments]);
    }

    public function postLikes($id)
    {
        $post = PostsController::getPost($id);
        $likesCount = sizeof($post->likes);
        return response(["Post Title:" => $post->title, "Likes" => $likesCount]);
    }
}
