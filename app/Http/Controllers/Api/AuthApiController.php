<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Controller;
use App\Http\Requests\SignInRequest;
use App\Http\Requests\SignUpRequest;

class AuthApiController extends Controller
{
    public function signUp(SignUpRequest $signUpRequest)
    {
        AuthController::createNewUser($signUpRequest->get("email"), $signUpRequest->get("password"));
        return response(["message" => "User " . $signUpRequest->get("email") . " has been created successfully"]);
    }

    public function signIn(SignInRequest $signInRequest)
    {
        if (!auth()->attempt($signInRequest->all())) {
            return response(["message" => "Credentials are not correct, try again!"]);
        }
        $token = auth()->user()->createToken("Api Token")->accessToken;

        return response(["Token" => $token]);
    }
}
