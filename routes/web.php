<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/signin', [AuthController::class, "signIn"])->name("signin");
Route::post('/signin-user', [AuthController::class, "signInUser"])->name("signin.user");
Route::get('/signup', [AuthController::class, "signUp"])->name("signup");
Route::post("signup-user", [AuthController::class, "signUpUser"])->name("signup.user");
Route::get("/", [PostsController::class, "posts"]);
Route::get("/signout", [PostsController::class, "signOut"])->name("signout.user");
Route::get("/create-post", [PostsController::class, "createPost"]);
Route::get("/my-posts", [PostsController::class, "myPosts"]);
Route::post("/save-post", [PostsController::class, "savePost"])->name("save.post");
Route::delete("/delete-post/{id}", [PostsController::class, "deletePost"])->name("delete.post");
Route::get("/edit-post/{id}", [PostsController::class, "editPost"])->name("edit.post");
Route::post("/save-edited-post/{id}", [PostsController::class, "saveEditedPost"])->name("save.edited.post");
Route::get("/post-details/{id}", [PostsController::class, "postDetails"]);
Route::post("/send-comment", [PostsController::class, "sendComment"])->name("send.comment");
Route::get("/post-like/{id}", [PostsController::class, "likePost"])->name("like.post");
Route::get("/post-dislike/{id}", [PostsController::class, "dislikePost"])->name("dislike.post");
